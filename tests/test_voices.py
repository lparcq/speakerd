# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import unittest

import speakerd.config

from speakerd.output.voices import *

class TestServer(unittest.TestCase):

    config = speakerd.config.ConfigData({ 'en': { 'FEMALE1': 'en-1', 'MALE1': 'en-2' },
                                          'fr': { 'FEMALE1': 'fr-1', 'MALE1': 'fr-2' } })

    def test_get_voice(self):
        """Get a voice."""
        mgr = VoiceManager()
        mgr.initialize(self.config)
        voice = mgr.voice('en-2')
        self.assertEqual(voice.name, 'en-2')
        self.assertEqual(voice.gender, 'M')
        with self.assertRaises(VoiceNotFoundError):
            mgr.voice('not_a_voice')

    def test_get_main_voice(self):
        """Get the main voice of a language."""
        mgr = VoiceManager()
        mgr.initialize(self.config)
        voice_info = mgr.main_voice_for_language('en')
        self.assertIn(voice_info.voice.gender, ['F', 'M', '-'])
        self.assertIn(voice_info.logical_name, ['FEMALE1', 'MALE1'])
        with self.assertRaises(VoiceNotFoundError):
            mgr.main_voice_for_language('not_a_language')

    def test_get_logical_voice(self):
        """Get the logical voice of a language."""
        mgr = VoiceManager()
        mgr.initialize(self.config)
        for logical_name in ['FEMALE1', 'MALE1']:
            voice = mgr.logical_voice('en', logical_name)
            self.assertEqual(voice.gender, logical_name[0], "{}: invalid logical name: {}".format(voice.name, logical_name))
        with self.assertRaises(VoiceNotFoundError):
            mgr.logical_voice('not_a_language', 'en-1')
        with self.assertRaises(VoiceNotFoundError):
            mgr.logical_voice('en', 'not_a_voice')
        for language in mgr.languages():
            for name in mgr.MANDATORY_LOGICAL_VOICES:
                mgr.logical_voice(language, name)

        logical_name = mgr.logical_name('en', 'en-1')
        self.assertIn(logical_name, mgr.MANDATORY_LOGICAL_VOICES)

    def test_get_voices_by_language(self):
        "Get all voices grouped by language"
        mgr = VoiceManager()
        mgr.initialize(self.config)
        voices = mgr.voices_by_language()
        self.assertIn('en', voices)
        self.assertIn('MALE1', voices['en'])

    def test_logical_name_sort_key(self):
        """Sort key for logical name"""
        for name, key in [ ('CHILD_FEMALE', '000CHILD_FEMALE'), ('CHILD_MALE', '000CHILD_MALE'),
                           ('FEMALE1', '001FEMALE'), ('MALE1', '001MALE') ]:
            self.assertEqual(key, VoiceManager.logical_name_sort_key(name))

    def test_initialize_from_config(self):
        """Initialize from config."""
        mgr = VoiceManager()
        mgr.initialize(self.config)
        voice_info = mgr.main_voice_for_language('en')
        self.assertIn(voice_info.voice.name, [ 'en-1', 'en-2' ])
        self.assertEqual(voice_info.voice.name[-1], voice_info.logical_name[-1])

        config = self.config.as_dict()
        for language in config:
            for logical_name in config[language]:
                voice = mgr.logical_voice(language, logical_name)
                self.assertEqual(config[language][logical_name], voice.name)

if __name__ == '__main__':
    unittest.main()
