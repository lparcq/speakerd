# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import os
import shutil
import tempfile
import unittest
import unittest.mock

import speakerd.config

from speakerd.client import OutputParameters
from speakerd.output.pico import PlayBack, Voices
from speakerd.output.voices import VoiceManager

pico_path = None

def find_pico():
    for dirname in os.environ['PATH'].split(os.pathsep):
        path = os.path.join(dirname, 'pico2wave')
        if os.path.exists(path):
            return path
    return None

pico_path = find_pico()

@unittest.skipIf(not pico_path, "pico not available")
class TestVoices(unittest.TestCase):

    def test_list_voices(self):
        """List voices."""
        voices = Voices(speakerd.config.ConfigData()).load()
        by_name = {}
        for voice in voices:
            by_name[voice.name] = voice
        self.assertIn('en-US', by_name)

@unittest.skipIf(not pico_path, "pico not available")
class TestPlayback(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()
        self.cache_dir = os.path.join(self.working_dir, 'cache')
        os.mkdir(self.cache_dir)

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def new_message(self, text):
        parameters = OutputParameters(language='en', physical_voice='en-US')
        return speakerd.client.Message(text.encode('UTF-8'), parameters)

    def test_play(self):
        """Play a message"""
        config = speakerd.config.ConfigData({ 'cache': { 'directory': self.cache_dir, 'max_entries': 10 } })
        with unittest.mock.patch.object(speakerd.output.pico.PlayBack, 'spawn_process', return_value=(io.BytesIO(), None)) as mock_method:
            playback = PlayBack(config, VoiceManager())
            try:
                msg = self.new_message('hello')
                playback.say(msg)
                self.assertIsNotNone(mock_method.call_args)
                cmdline = [ pico_path, '-l', 'en-US', '-w', mock_method.call_args[0][0][-2], msg.data ]
                mock_method.assert_called_once_with(cmdline)
            finally:
                playback.stop()

if __name__ == '__main__':
    unittest.main()
