# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import os
import re
import shutil
import tempfile
import unittest
import unittest.mock

import speakerd.config

from speakerd.client import OutputParameters
from speakerd.output.espeak import PlayBack, Voices
from speakerd.output.voices import VoiceManager

espeak_path = None

def find_espeak():
    for dirname in os.environ['PATH'].split(os.pathsep):
        path = os.path.join(dirname, 'espeak')
        if os.path.exists(path):
            return path
    return None

espeak_path = find_espeak()

@unittest.skipIf(not espeak_path, "espeak not available")
class TestVoices(unittest.TestCase):

    def test_list_voices(self):
        """List voices."""
        voices = Voices(speakerd.config.ConfigData()).load()
        by_name = {}
        for voice in voices:
            by_name[voice.name] = voice
        sep_re = re.compile('[_-]')
        shord_names = set([ sep_re.split(name)[0] for name in by_name ])
        self.assertIn('english', shord_names)

@unittest.skipIf(not espeak_path, "espeak not available")
class TestPlayback(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()
        self.cache_dir = os.path.join(self.working_dir, 'cache')
        os.mkdir(self.cache_dir)

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def new_message(self, text):
        parameters = OutputParameters(language='en', physical_voice='english')
        return speakerd.client.Message(text.encode('UTF-8'), parameters)

    def test_conversions(self):
        """Convert SSPI values to espeak"""
        volume = PlayBack.convert_volume(OutputParameters(volume=OutputParameters.MIN_VOLUME))
        self.assertEqual(PlayBack.ESPEAK_MIN_AMPLITUDE, volume)
        volume = PlayBack.convert_volume(OutputParameters(volume=OutputParameters.MAX_VOLUME))
        self.assertEqual(PlayBack.ESPEAK_MAX_AMPLITUDE, volume)
        pitch = PlayBack.convert_pitch(OutputParameters(pitch=OutputParameters.MIN_PITCH))
        self.assertEqual(PlayBack.ESPEAK_MIN_PITCH, pitch)
        pitch = PlayBack.convert_pitch(OutputParameters(pitch=OutputParameters.MAX_PITCH))
        self.assertEqual(PlayBack.ESPEAK_MAX_PITCH, pitch)
        rate = PlayBack.convert_rate(OutputParameters(rate=OutputParameters.MIN_RATE))
        self.assertEqual(PlayBack.ESPEAK_MIN_SPEED, rate)
        rate = PlayBack.convert_rate(OutputParameters(rate=OutputParameters.MAX_RATE))
        self.assertEqual(PlayBack.ESPEAK_MAX_SPEED, rate)

    def test_play(self):
        """Play a message"""
        config = speakerd.config.ConfigData({ 'cache': { 'directory': self.cache_dir, 'max_entries': 10 } })
        with unittest.mock.patch.object(speakerd.output.espeak.PlayBack, 'spawn_process', return_value=(io.BytesIO(), None)) as mock_method:
            playback = PlayBack(config, VoiceManager(), quiet=True)
            try:
                msg = self.new_message('hello')
                playback.say(msg)
                cmdline = [espeak_path, '-z', '-a', '200', '-p', '50', '-s', str(PlayBack.ESPEAK_DEFAULT_SPEED), '-v', 'english', '-q']
                mock_method.assert_called_once_with(cmdline, True)
            finally:
                playback.stop()

if __name__ == '__main__':
    unittest.main()
