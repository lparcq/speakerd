# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd (at) circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import locale
import logging
import logging.handlers
import os
import shutil
import tempfile
import unittest
import unittest.mock

import speakerd.client
import speakerd.config
import speakerd.output
import speakerd.msgs
from speakerd.client import OutputParameters, PriorityQueue, ClientHandler, find_command

def dummy_config(cache_dir=None):
    config = {}
    if cache_dir:
        config = { 'cache': { 'directory': cache_dir, 'max_entries': 100 } }
    isolang = locale.getlocale()[0]
    languages = { isolang[0:2]: isolang }
    if not 'en' in languages:
        languages['en'] = 'en_US'
    voices = {}
    for language in languages:
        isolang = languages[language]
        voices[language] = { 'FEMALE1': isolang + '-f', 'MALE1': isolang + '-m' }
    config.update({ 'output': { 'module': 'dummy' },
                    'modules': { 'dummy': { 'voices': voices },
                                 'espeak': { 'voices': {} },
                                 'pico': { 'voices': {} } } })
    return speakerd.config.ConfigData(config)

def available_modules():
    available_modules = [ 'dummy' ]
    for name, executable in [('espeak', 'espeak'), ('pico', 'pico2wave')]:
        if find_command(executable):
            available_modules.append(name)
    available_modules.sort()
    return available_modules

class TestPriorityQueue(unittest.TestCase):

    def new_item(self, priority, client_number=0):
        return speakerd.client.MessageBlock(client_number, priority, True)

    def check_priorities(self, queue, *priorities, **kw):
        title = None
        if 'title' in kw:
            title = kw['title']
        self.assertEqual(len(queue.queue), len(priorities), title)
        for index, priority in enumerate(priorities):
            self.assertEqual(queue.queue[index].priority, priority, title)

    def check_clients(self, queue, *clients, **kw):
        title = None
        if 'title' in kw:
            title = kw['title']
        self.assertEqual(len(queue.queue), len(clients), title)
        for index, client_number in enumerate(clients):
            self.assertEqual(queue.queue[index].client_number, client_number, title)

    def test_progress(self):
        """Progress message."""
        # progress is added in an empty queue
        queue = PriorityQueue()
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_PROGRESS))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_PROGRESS)

        # progress not added if spoken message with higher priority
        queue = PriorityQueue()
        queue.current = self.new_item(PriorityQueue.PRY_TEXT)
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_PROGRESS))
        self.assertFalse(canceled)
        self.check_priorities(queue)

        # progress not added in queue with message with higher priority
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_TEXT))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_PROGRESS))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_TEXT)

        # progress replaces the previous one
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_PROGRESS))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_PROGRESS, 1))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_PROGRESS)
        self.check_clients(queue, 1)

        # a progress message cancels all other progress messages except the current one
        queue = PriorityQueue()
        queue.current = self.new_item(PriorityQueue.PRY_PROGRESS)
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_PROGRESS))
        self.assertFalse(canceled)
        self.assertIsNotNone(queue.current)
        self.check_priorities(queue, PriorityQueue.PRY_PROGRESS)

    def test_notification(self):
        """Notification message."""
        # notification is added in an empty queue
        queue = PriorityQueue()
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_NOTIFICATION)

        # notification not added if spoken message with higher priority
        queue = PriorityQueue()
        queue.current = self.new_item(PriorityQueue.PRY_TEXT)
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        self.assertFalse(canceled)
        self.check_priorities(queue)

        # notification not added in queue with message with higher priority
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_TEXT))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_TEXT)

        # notification replaces the previous one
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_NOTIFICATION, 1))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_NOTIFICATION)
        self.check_clients(queue, 1)

    def test_text(self):
        """Text message."""
        # cancel notification and progress
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        queue.queue.append(self.new_item(PriorityQueue.PRY_PROGRESS))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_TEXT))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_TEXT)

        # keep only last text
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_MESSAGE))
        queue.queue.append(self.new_item(PriorityQueue.PRY_TEXT))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_TEXT, 1))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_MESSAGE, PriorityQueue.PRY_TEXT)
        self.check_clients(queue, 0, 1)

        # cancel current text
        for priority in [ PriorityQueue.PRY_TEXT, PriorityQueue.PRY_NOTIFICATION, PriorityQueue.PRY_PROGRESS ]:
            queue = PriorityQueue()
            queue.current = self.new_item(priority)
            canceled = queue.insert(self.new_item(PriorityQueue.PRY_TEXT, 1))
            self.assertTrue(canceled)
            self.check_priorities(queue, PriorityQueue.PRY_TEXT)
            self.check_clients(queue, 1)

    def test_message(self):
        """'Message' message."""
        # cancel notification and progress
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        queue.queue.append(self.new_item(PriorityQueue.PRY_PROGRESS))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_MESSAGE))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_MESSAGE)

        # insert before text
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_MESSAGE))
        queue.queue.append(self.new_item(PriorityQueue.PRY_TEXT))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_MESSAGE, 1))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_MESSAGE, PriorityQueue.PRY_MESSAGE, PriorityQueue.PRY_TEXT)
        self.check_clients(queue, 0, 1, 0)

        # cancel current lower priority
        for priority in [ PriorityQueue.PRY_TEXT, PriorityQueue.PRY_NOTIFICATION, PriorityQueue.PRY_PROGRESS ]:
            queue = PriorityQueue()
            queue.current = self.new_item(priority)
            canceled = queue.insert(self.new_item(PriorityQueue.PRY_MESSAGE, 1))
            self.assertTrue(canceled)
            title = "cancel current lower priority than {}".format(priority)
            self.check_priorities(queue, PriorityQueue.PRY_MESSAGE, title=title)
            self.check_clients(queue, 1, title=title)

        # don't cancel current message
        for priority in [ PriorityQueue.PRY_IMPORTANT, PriorityQueue.PRY_MESSAGE ]:
            queue = PriorityQueue()
            queue.current = self.new_item(priority)
            canceled = queue.insert(self.new_item(PriorityQueue.PRY_MESSAGE, 1))
            self.assertFalse(canceled)
            self.check_priorities(queue, PriorityQueue.PRY_MESSAGE)
            self.check_clients(queue, 1)

    def test_important(self):
        """Important message."""
        # cancel notification and progress
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_NOTIFICATION))
        queue.queue.append(self.new_item(PriorityQueue.PRY_PROGRESS))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_IMPORTANT))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_IMPORTANT)

        # insert before message
        queue = PriorityQueue()
        queue.queue.append(self.new_item(PriorityQueue.PRY_MESSAGE))
        queue.queue.append(self.new_item(PriorityQueue.PRY_TEXT))
        canceled = queue.insert(self.new_item(PriorityQueue.PRY_IMPORTANT, 1))
        self.assertFalse(canceled)
        self.check_priorities(queue, PriorityQueue.PRY_IMPORTANT, PriorityQueue.PRY_MESSAGE, PriorityQueue.PRY_TEXT)
        self.check_clients(queue, 1, 0, 0)

        # cancel current lower priority
        for priority in [ PriorityQueue.PRY_MESSAGE, PriorityQueue.PRY_TEXT, PriorityQueue.PRY_NOTIFICATION, PriorityQueue.PRY_PROGRESS ]:
            queue = PriorityQueue()
            queue.current = self.new_item(priority)
            canceled = queue.insert(self.new_item(PriorityQueue.PRY_IMPORTANT, 1))
            self.assertTrue(canceled)
            title = "cancel current lower priority than {}".format(priority)
            self.check_priorities(queue, PriorityQueue.PRY_IMPORTANT, title=title)
            self.check_clients(queue, 1, title=title)

        # don't cancel current message
        for priority in [ PriorityQueue.PRY_IMPORTANT ]:
            queue = PriorityQueue()
            queue.current = self.new_item(priority)
            canceled = queue.insert(self.new_item(PriorityQueue.PRY_MESSAGE, 1))
            self.assertFalse(canceled)
            self.check_priorities(queue, PriorityQueue.PRY_MESSAGE)
            self.check_clients(queue, 1)

class TestOutputParameters(unittest.TestCase):

    def setUp(self):
        self.working_dir = tempfile.mkdtemp()
        cache_dir = os.path.join(self.working_dir, 'cache')
        OutputParameters.initialize_modules(dummy_config(cache_dir=cache_dir))

    def tearDown(self):
        OutputParameters.clear_modules()
        shutil.rmtree(self.working_dir)

    def test_default_language(self):
        """Default language"""
        parameters = OutputParameters()
        parameters.set_default_language()
        self.assertEqual(len(parameters.language), 2)
        self.assertEqual(parameters.language.lower(), parameters.language)


class FakePlayBack:

    """Replacement for a playback module.

    Each time proceed is called, the step number increases. The constructor takes a
    list of steps as arguments that represents when the player is busy."""

    def __init__(self, busy_steps):
        self.step = 0
        self.busy_steps = busy_steps
        self.said_messages = []
        self.calls = { 'stop': 0, 'pause': 0, 'resume': 0 }
        self.voice_manager = speakerd.output.VoiceManager()
        self.voice_manager.initialize(dummy_config().get('modules', 'dummy', 'voices'))

    def proceed(self):
        """Do any pending action and return if idle."""
        self.step += 1
        while self.busy_steps and self.step > self.busy_steps[0]:
            self.busy_steps.pop(0)
        # If the list of idle steps is empty, the player is considered idle. Otherwise
        # it is only idle of the specified steps
        return not self.busy_steps or self.step < self.busy_steps[0]

    def say(self, msg):
        """Say the message"""
        self.said_messages.append((self.step, msg.data))

    def stop(self):
        self.calls['stop'] += 1

    def pause(self):
        self.calls['pause'] += 1

    def resume(self):
        self.calls['resume'] += 1

class TestClientHandler(unittest.TestCase):

    CLIENT_ID = 0

    def setUp(self):
        root = logging.getLogger()
        root.handlers = []
        root.addHandler(logging.NullHandler())
        self.working_dir = tempfile.mkdtemp()
        self.cache_dir = os.path.join(self.working_dir, 'cache')

    def tearDown(self):
        root = logging.getLogger()
        if root:
            for hdlr in [ hdlr for hdlr in root.handlers ]:
                hdlr.close()
                root.removeHandler(hdlr)
        shutil.rmtree(self.working_dir)

    def run_commands(self, commands, expected_result, started_clients=1, stopped_client=0, playback=None):
        """Run a simple command and test output.

        STARTED_CLIENTS is the number of clients in parallel. Only one of then handles the commands. The
        others are only started.

        STOPPED_CLIENT is the number of clients that are supposed to stop during the processing, for
        example because of a QUIT.

        PLAYBACK is an alternative playback instance."""
        self.assertEqual(len(speakerd.client.OutputParameters.modules), 0)
        config = dummy_config(cache_dir=self.cache_dir)
        with ClientHandler(config) as handler:
            for client_index in range(started_clients-1):
                rfile = io.BytesIO('HELP\n'.encode('ASCII'))
                wfile = io.BytesIO()
                handler.handle(self.CLIENT_ID + client_index + 1, rfile, wfile)
            self.assertEqual(started_clients - 1, len(handler.clients))
            original_playback = None
            if playback: # specific playback instance for the client running the commands
                speakerd.client.OutputParameters.modules['dummy'] = playback
            wfile = io.BytesIO()
            # send commands one by one
            for command in commands:
                rfile = io.BytesIO('{}\r\n'.format(command).encode('ASCII'))
                handler.handle(self.CLIENT_ID, rfile, wfile)
            # check the number of clients
            self.assertEqual(started_clients - stopped_client, len(handler.clients))
            # check the answers
            result = wfile.getvalue().decode('ASCII').rstrip().split('\r\n')
            if isinstance(expected_result, list):
                self.assertEqual(expected_result, result, "error for command: {}".format(command))
            else:
                expected_result(result)

    def run_command(self, command, expected_result, started_clients=1, stopped_client=0):
        """Run a simple command and test output."""
        self.run_commands([ command ], expected_result, started_clients=started_clients, stopped_client=stopped_client)

    def test_block_set_parameter(self):
        """BLOCK set parameters"""
        self.run_commands(["BLOCK BEGIN",
                           "SET self RATE 0",
                           "SET self PITCH 0",
                           "SET self VOLUME 0",
                           "SET self VOICE_TYPE MALE1",
                           "SET self LANGUAGE en",
                           "SET self PUNCTUATION none",
                           "SET self CAP_LET_RECOGN none",
                           "BLOCK END"],
                          ['260 OK INSIDE BLOCK',
                           '203 OK RATE SET',
                           '204 OK PITCH SET',
                           '218 OK VOLUME SET',
                           '209 OK VOICE SET',
                           '201 OK LANGUAGE SET',
                           '205 OK PUNCTUATION SET',
                           '206 OK CAP LET RECOGNITION SET',
                           '261 OK OUTSIDE BLOCK'])

    def test_block_forbidden_commands(self):
        """BLOCK forbidden commands"""
        self.run_commands(["BLOCK BEGIN",
                           "SET all RATE 0",
                           "SET all PITCH 0",
                           "SET all VOLUME 0",
                           "SET all VOICE_TYPE MALE1",
                           "SET all LANGUAGE en",
                           "SET all PUNCTUATION none",
                           "SET all CAP_LET_RECOGN none",
                           "GET OUTPUT_MODULE",
                           "STOP self",
                           "BLOCK END"],
                          ['260 OK INSIDE BLOCK'] + ['332 ERR NOT ALLOWED INSIDE BLOCK']*9 + ['261 OK OUTSIDE BLOCK'],
                          started_clients=2)

    def test_block_speak(self):
        """BLOCK + SPEAK"""
        with unittest.mock.patch.object(speakerd.output.dummy.PlayBack, 'say') as say_method:
            self.run_commands(["BLOCK BEGIN",
                               "SET self VOICE_TYPE MALE1",
                               "SPEAK\r\nquestion\r\n.",
                               "SET self VOICE_TYPE FEMALE1",
                               "SPEAK\r\nanswer\r\n.",
                               "BLOCK END"],
                              ['260 OK INSIDE BLOCK',
                               '209 OK VOICE SET',
                               '230 OK RECEIVING DATA',
                               '225-1',
                               '225 OK MESSAGE QUEUED',
                               '209 OK VOICE SET',
                               '230 OK RECEIVING DATA',
                               '225-2',
                               '225 OK MESSAGE QUEUED',
                               '261 OK OUTSIDE BLOCK'])
            messages = [b"question\r\n", b"answer\r\n"]
            self.assertEqual(len(messages), len(say_method.call_args_list), 'invalid call args list: {}'.format(say_method.call_args_list))
            for index, msg in enumerate(messages):
                args, kw = say_method.call_args_list[index]
                arg = args[0]
                self.assertIsInstance(arg, speakerd.client.Message)
                self.assertEqual(msg, arg.data)

    def test_block_key(self):
        """BLOCK + KEY"""
        with unittest.mock.patch.object(speakerd.output.dummy.PlayBack, 'say') as say_method:
            key_sets = ['vdljzwa', 'uie,yx.k']
            commands = ['SET self LANGUAGE en', 'BLOCK BEGIN']
            answers = ['201 OK LANGUAGE SET', '260 OK INSIDE BLOCK']
            for kset in key_sets:
                for key in kset:
                    commands.append('KEY {}'.format(key))
                    answers.append('225 OK MESSAGE QUEUED')
            commands.append('BLOCK END')
            answers.append('261 OK OUTSIDE BLOCK')
            self.run_commands(commands, answers)
            all_keys = ''.join(key_sets)
            self.assertEqual(len(all_keys), len(say_method.call_args_list))
            for index, key in enumerate(all_keys):
                args, kw = say_method.call_args_list[index]
                arg = args[0]
                self.assertIsInstance(arg, speakerd.client.Message)
                if key == ',':
                    key = 'comma'
                elif key == '.':
                    key = 'dot'
                self.assertEqual(key.encode('ASCII'), arg.data)

    def test_block_with_long_playback(self):
        """BLOCK with long playback"""
        with unittest.mock.patch.object(speakerd.output.dummy.PlayBack, 'say') as say_method:
            playback = FakePlayBack([1])
            self.run_commands(["BLOCK BEGIN",
                               "KEY a", "KEY b", "KEY c",
                               "BLOCK END"],
                              ['260 OK INSIDE BLOCK',
                               '225 OK MESSAGE QUEUED', '225 OK MESSAGE QUEUED', '225 OK MESSAGE QUEUED',
                               '261 OK OUTSIDE BLOCK'],
                              playback=playback)
            self.assertEqual([(0, b'a'), (2, b'b'), (3, b'c')], playback.said_messages)

    def test_help(self):
        """HELP"""
        self.run_command("HELP", [speakerd.msgs.ERR_NOT_IMPLEMENTED])

    def test_history(self):
        """HISTORY"""
        self.run_command("HISTORY GET CLIENT_LIST", [speakerd.msgs.ERR_NOT_IMPLEMENTED])

    def test_list_output_modules(self):
        """LIST OUTPUT_MODULES"""
        result = [ '250-{}'.format(name) for name in available_modules() ]
        self.run_command("LIST OUTPUT_MODULES", result + [ "250 OK MODULE LIST SENT"])

    def check_list_synthesis_voices(self, output):
        for line in output:
            if line == '249 OK VOICE LIST SENT':
                continue
            self.assertTrue(line.startswith('249-'), "line not starting with 249-: {}".format(line))
            tokens = line[4:].split()
            self.assertEqual(len(tokens), 3, "invalid line: {}".format(line))
            variant = tokens[-1].split('-')
            if len(variant) == 1:
                self.assertIn(variant[0], ['none', 'jbo', 'lfn', 'grc'])
            elif len(variant) == 2:
                self.assertEqual(len(variant[0]), 2, "invalid variant: {}".format(variant))
            else:
                self.assertEqual(len(variant), 3, "invalid variant: {}".format(variant))

    def test_list_synthesis_voices(self):
        """LIST SYNTHESIS_VOICES"""
        self.run_command("LIST SYNTHESIS_VOICES", self.check_list_synthesis_voices)

    def test_list_voices(self):
        """LIST VOICES"""
        self.run_command("LIST VOICES", ['249-CHILD_FEMALE', '249-CHILD_MALE',
                                         '249-FEMALE1', '249-FEMALE2', '249-FEMALE3',
                                         '249-MALE1', '249-MALE2', '249-MALE3', '249 OK VOICE LIST SENT'])

    def test_key(self):
        """KEY"""
        playback = FakePlayBack([])
        self.run_commands(["SET self PRIORITY text", "KEY a",
                           "SET self PRIORITY notification", "KEY b", "KEY c"],
                          ['202 OK PRIORITY SET', '225 OK MESSAGE QUEUED',
                           '202 OK PRIORITY SET', '225 OK MESSAGE QUEUED', '225 OK MESSAGE QUEUED'],
                          playback=playback)
        self.assertEqual([(0, b'a'), (1, b'b'), (2, b'c')], playback.said_messages)

    def test_speak(self):
        """SPEAK"""
        message = "message\r\n"
        with unittest.mock.patch.object(speakerd.output.dummy.PlayBack, 'say') as say_method:
            self.run_commands(["SPEAK\r\n" + message + "."],
                              ['230 OK RECEIVING DATA', '225-1', '225 OK MESSAGE QUEUED'])
            self.assertIsInstance(say_method.call_args[0][0], speakerd.client.Message)
            self.assertEqual(message.encode('ASCII'), say_method.call_args[0][0].data)

    def test_speak_priority(self):
        """SPEAK with priority"""
        playback=FakePlayBack([1, 2])
        self.run_commands(["SPEAK\r\nfirst message\r\n.",
                           "SET self PRIORITY important\r\nSPEAK\r\nsecond message\r\n."],
                          ['230 OK RECEIVING DATA', '225-1', '225 OK MESSAGE QUEUED',
                           '202 OK PRIORITY SET',
                           '230 OK RECEIVING DATA', '225-2', '225 OK MESSAGE QUEUED'],
                          playback=playback)
        self.assertEqual(2, playback.calls['stop']) # one to cancel message, one to close client

    def test_stop(self):
        """STOP"""
        self.run_command("STOP self", ['210 OK STOPPED'])

    def test_cancel(self):
        """CANCEL"""
        self.run_command("CANCEL self", ['213 OK CANCELED'])

    def test_pause(self):
        """PAUSE"""
        self.run_command("PAUSE self", ['211 OK PAUSED'])

    def test_resume(self):
        """RESUME"""
        self.run_command("RESUME self", ['212 OK RESUMED'])

    def test_set_cap_let_recogn(self):
        """SET CAP_LET_RECOGN"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            self.run_command('SET {} CAP_LET_RECOGN none'.format(target), ['206 OK CAP LET RECOGNITION SET'])
            for value in [ 'spell', 'icon' ]:
                self.run_command('SET {} CAP_LET_RECOGN {}'.format(target, value), [speakerd.msgs.ERR_NOT_IMPLEMENTED])

    def test_set_client_name(self):
        """SET CLIENT_NAME"""
        client_name = 'test:1'
        self.run_command('SET self CLIENT_NAME {}'.format(client_name), ['208 OK CLIENT NAME SET'])
        self.run_command('SET all CLIENT_NAME name', ['311 ERR COULDNT SET CLIENT_NAME'], started_clients=2)

    def test_set_debug(self):
        """SET DEBUG"""
        self.run_command('SET self DEBUG ON', ['317 ERR COULDNT SET DEBUGGING'])
        self.run_command('SET all DEBUG ON', ['262 OK DEBUGGING SET'])
        self.run_command('SET all DEBUG OFF', ['262 OK DEBUGGING SET'])

    def test_set_history(self):
        """SET HISTORY"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            self.run_command('SET {} HISTORY unknown'.format(target), ['513 ERR PARAMETER NOT ON OR OFF'])
            self.run_command('SET {} HISTORY on'.format(target), [speakerd.msgs.ERR_NOT_IMPLEMENTED ])
            self.run_command('SET {} HISTORY off'.format(target), [speakerd.msgs.ERR_NOT_IMPLEMENTED ])

    def test_set_language(self):
        """SET LANGUAGE"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            self.run_command('SET {} LANGUAGE unknown'.format(target), ['302 ERR COULDNT SET LANGUAGE'])
            self.run_command('SET {} LANGUAGE en'.format(target), ['201 OK LANGUAGE SET'])
        self.run_command('SET xxx LANGUAGE espeak', [speakerd.msgs.ERR_PARAMETER_INVALID])

    def test_set_notification(self):
        """SET NOTIFICATION"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in [ 'ALL', 'BEGIN', 'END', 'CANCEL', 'PAUSE', 'RESUME', 'INDEX_MARKS' ]:
                self.run_command('SET {} NOTIFICATION {} unknown'.format(target, value), ['513 ERR PARAMETER NOT ON OR OFF'])
                self.run_command('SET {} NOTIFICATION {} on'.format(target, value), [speakerd.msgs.ERR_NOT_IMPLEMENTED ])
                self.run_command('SET {} NOTIFICATION {} off'.format(target, value), [speakerd.msgs.ERR_NOT_IMPLEMENTED ])

    def test_get_output_module(self):
        """GET OUTPUT_MODULE"""
        self.run_command('GET OUTPUT_MODULE', ['251-dummy', '251 OK GET RETURNED'])

    def test_set_output_module(self):
        """SET OUTPUT_MODULE"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            self.run_command('SET {} OUTPUT_MODULE unknown'.format(target), ['312 ERR COULDNT SET OUTPUT MODULE'])
            self.run_command('SET {} OUTPUT_MODULE espeak'.format(target), ['216 OK OUTPUT MODULE SET'])
        self.run_command('SET xxx OUTPUT_MODULE espeak', [speakerd.msgs.ERR_PARAMETER_INVALID])

    def test_set_pause_context(self):
        """SET PAUSE_CONTEXT"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            self.run_command('SET {} PAUSE_CONTEXT 0'.format(target), [speakerd.msgs.ERR_NOT_IMPLEMENTED])

    def test_get_pitch(self):
        """GET PITCH"""
        self.run_command('GET PITCH', ['251-0', '251 OK GET RETURNED'])

    def test_set_pitch(self):
        """SET PITCH"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in [-100, 0, 100]:
                self.run_command('SET {} PITCH {}'.format(target, value), ['204 OK PITCH SET'])
            self.run_command('SET {} PITCH -101'.format(target), ['412 ERR PITCH TOO LOW'])
            self.run_command('SET {} PITCH 101'.format(target), ['411 ERR PITCH TOO HIGH'])
            self.run_command('SET {} PITCH unknown'.format(target), ['304 ERR COULDNT SET PITCH'])

    def test_set_priority(self):
        """SET PRIORITY"""
        for priority in ['important', 'text', 'message', 'notification', 'progress']:
            self.run_command('SET self PRIORITY {}'.format(priority), ['202 OK PRIORITY SET'])
        self.run_command('SET all PRIORITY text', ['301 ERR COULDNT SET PRIORITY'], started_clients=2)
        self.run_command('SET self PRIORITY high', ['408 ERR UNKNOWN PRIORITY'])
        self.run_command('SET xxx PRIORITY text', [speakerd.msgs.ERR_PARAMETER_INVALID])

    def test_set_punctuation(self):
        """SET PUNCTUATION"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in [ 'all', 'some', 'none' ]:
                self.run_command('SET {} PUNCTUATION {}'.format(target, value), ['205 OK PUNCTUATION SET'])
            self.run_command('SET {} PUNCTUATION unknown'.format(target), ['305 ERR COULDNT SET PUNCT MODE'])

    def test_get_rate(self):
        """GET RATE"""
        self.run_command('GET RATE', ['251-0', '251 OK GET RETURNED'])

    def test_set_rate(self):
        """SET RATE"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in [-100, 0, 100]:
                self.run_command('SET {} RATE {}'.format(target, value), ['203 OK RATE SET'])
            self.run_command('SET {} RATE -101'.format(target), ['410 ERR RATE TOO LOW'])
            self.run_command('SET {} RATE 101'.format(target), ['409 ERR RATE TOO HIGH'])
            self.run_command('SET {} RATE unknown'.format(target), ['303 ERR COULDNT SET RATE'])

    def test_set_spelling(self):
        """SET SPELLING"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            self.run_command('SET {} SPELLING unknown'.format(target), ['513 ERR PARAMETER NOT ON OR OFF'])
            self.run_command('SET {} SPELLING on'.format(target), ['207 OK SPELLING SET'])
            self.run_command('SET {} SPELLING off'.format(target), ['207 OK SPELLING SET'])

    def test_set_ssml_mode(self):
        """SET SSML_MODE"""
        for target in [ 'self', self.CLIENT_ID ]:
            self.run_command('SET {} SSML_MODE unknown'.format(target), ['513 ERR PARAMETER NOT ON OR OFF'])
            self.run_command('SET {} SSML_MODE on'.format(target), ['219 OK SSML MODE SET'])
            self.run_command('SET {} SSML_MODE off'.format(target), ['219 OK SSML MODE SET'])

    def test_set_synthesis_voice(self):
        """SET SYNTHESIS_VOICE"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in ['en_US-f', 'en_US-m']:
                self.run_command('SET {} SYNTHESIS_VOICE {}'.format(target, value), ['209 OK VOICE SET'])
            self.run_command('SET {} SYNTHESIS_VOICE unknown'.format(target), ['309 ERR COULDNT SET VOICE'])

    def test_get_voice_type(self):
        """GET VOICE_TYPE"""
        self.run_command('GET VOICE_TYPE', ['251-FEMALE1', '251 OK GET RETURNED'])

    def test_set_voice_type(self):
        """SET VOICE_TYPE"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in ['CHILD_FEMALE', 'CHILD_MALE',
                          'FEMALE1', 'FEMALE2', 'FEMALE3',
                          'MALE1', 'MALE2', 'MALE3']:
                self.run_command('SET {} VOICE_TYPE {}'.format(target, value), ['209 OK VOICE SET'])
                self.run_command('SET {} VOICE_TYPE {}'.format(target, value.lower()), ['209 OK VOICE SET'])
            self.run_command('SET {} VOICE_TYPE unknown'.format(target), ['309 ERR COULDNT SET VOICE'])

    def test_get_volume(self):
        """GET VOLUME"""
        self.run_command('GET VOLUME', ['251-100', '251 OK GET RETURNED'])

    def test_set_volume(self):
        """SET VOLUME"""
        for target in [ 'all', 'self', self.CLIENT_ID ]:
            for value in [-100, 0, 100]:
                self.run_command('SET {} VOLUME {}'.format(target, value), ['218 OK VOLUME SET'])
            self.run_command('SET {} VOLUME -101'.format(target), ['414 ERR VOLUME TOO LOW'])
            self.run_command('SET {} VOLUME 101'.format(target), ['413 ERR VOLUME TOO HIGH'])
            self.run_command('SET {} VOLUME unknown'.format(target), ['314 ERR COULDNT SET VOLUME'])

    def test_quit(self):
        """QUIT"""
        self.run_command('QUIT', ['231 HAPPY HACKING'], stopped_client=1)

if __name__ == '__main__':
    unittest.main()
