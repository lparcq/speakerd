# Run all unit tests:
#  make test
#
# Run one test, from test_NAME.py
#  make test-NAME
#
# Debug one test, from test_NAME.py
#  make debug-NAME

SRCS = $(wildcard */*.py scripts/*)

test:
	python -m unittest discover

test-%:
	env PYTHONPATH=$(PWD) python tests/test_$*.py

debug-%:
	env PYTHONPATH=$(PWD) python -m pdb tests/test_$*.py

TAGS: $(SRCS)
	etags $+

bdist:
	python setup.py bdist

sdist:
	python setup.py sdist

clean:
	-rm -rf *.egg-info build dist */__pycache__ */*/__pycache__ TAGS
	-rm dist/*.*
	-rm -rf build/*
