Speakerd
========

[![pipeline status](https://gitlab.com/lparcq/speakerd/badges/master/pipeline.svg)](https://gitlab.com/lparcq/speakerd/commits/master)
[![license](https://img.shields.io/badge/license-GPL3-lightgrey.svg)](https://gitlab.com/lp-accessibility/speakerd/raw/master/LICENSE)

This is an implementation of [Speech Dispatcher](https://devel.freebsoft.org/speechd) in python. The purpose is not to replace the original program but to use it for testing or [POC](https://en.wikipedia.org/wiki/Proof_of_concept).

**This program is not under active development**

Limitations:

- Only support [espeak](http://espeak.sourceforge.net/) as an external command for TTS.

- Doesn't implement HISTORY and event notifications.

- Depends on a command to play WAV files such as [aplay](https://en.wikipedia.org/wiki/Aplay).

Notes:

- If the same message is played twice, the output is cached to avoid calling the TTS command for frequent messages.

- A config file is generated the first time the command is run.
