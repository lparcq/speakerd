# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import collections
import importlib
import io
import locale
import logging
import os
import shlex

__all__ = [ 'ClientHandler', 'find_command' ]

from speakerd import msgs
from .config import ConfigData
from .output import *

class ProtocolError(Exception):

    def __init__(self, message, ssip_error):
        super().__init__(message)
        self.ssip_error = ssip_error

class Parameters:

    ENCODING = 'UTF-8'

    @classmethod
    def in_list(self, value, valid_values, cannot_set_error):
        if not value in valid_values:
            raise ProtocolError('{}: not in {}'.format(value, ', '.join(valid_values)), cannot_set_error)
        return value

    @classmethod
    def in_range(self, value, min_value, max_value, min_ssip_error, max_ssip_error, cannot_set_error):
        try:
            ivalue = int(value)
        except ValueError as ex:
            raise ProtocolError('{}: invalid value'.format(value), cannot_set_error) from ex
        if ivalue < min_value:
            raise ProtocolError('{}: is less than {}'.format(value, min_value), min_ssip_error)
        elif ivalue > max_value:
            raise ProtocolError('{}: is greater than {}'.format(value, max_value), max_ssip_error)
        return ivalue

    @classmethod
    def to_boolean(self, value):
        value = value.lower()
        if not value in ('on', 'off'):
            raise ProtocolError("{}: invalid boolean value".format(value), msgs.ERR_PARAMETER_NOT_ON_OFF)
        return value == 'on'

    @classmethod
    def from_boolean(self, value):
        return 'on' if value else 'off'

class OutputConstants:

    MIN_PITCH = -100
    MAX_PITCH = 100
    DEFAULT_PITCH = 0

    MIN_RATE = -100
    MAX_RATE = 100
    DEFAULT_RATE = 0

    MIN_VOLUME = -100
    MAX_VOLUME = 100
    DEFAULT_VOLUME = MAX_VOLUME

    CAP_LET_RECOGN_CHOICES = ('none', 'spell', 'icon')

    PUNCTUATION_CHOICES = ('none', 'some', 'all')

class OutputParameters(Parameters, OutputConstants):

    current_pitch = OutputConstants.DEFAULT_PITCH
    current_rate = OutputConstants.DEFAULT_RATE

    modules = {}

    default_module_name = None

    def __init__(self, module_name=None, cap_let_recogn=None, language=None,
                 pitch=None, rate=None, spelling=False,
                 ssml_mode=False, physical_voice=None, logical_voice=None, volume=None):
        self.module_name = module_name or self.default_module_name
        self.module = None
        self.cap_let_recogn = cap_let_recogn or self.CAP_LET_RECOGN_CHOICES[0]
        self.language = language
        self.pitch = pitch if pitch != None else self.current_pitch
        self.punctuation = self.PUNCTUATION_CHOICES[0]
        self.rate = rate if rate != None else self.current_rate
        self.spelling = spelling
        self.ssml_mode = ssml_mode
        self.physical_voice = physical_voice
        self.logical_voice = logical_voice
        self.volume = volume if volume else self.DEFAULT_VOLUME

    @classmethod
    def initialize_modules(self, config):
        if not self.modules:
            self.modules.update(available_outputs(config))
            self.default_module_name = config.get('output', 'module')
            if not self.default_module_name and self.modules:
                self.default_module_name = sorted(self.modules)[0]
        return self.modules

    @classmethod
    def clear_modules(self):
        self.modules = {}

    def clone(self):
        return OutputParameters(self.module_name, self.cap_let_recogn, self.language,
                                self.pitch, self.rate, self.spelling,
                                self.ssml_mode, self.physical_voice, self.logical_voice, self.volume)

    def get_module(self):
        if not self.module:
            self.module = self.modules[self.module_name]
        return self.module

    def set_module(self, module_name):
        if not module_name in self.modules:
            raise ProtocolError('{}: invalid output module'.format(module_name), msgs.ERR_COULDNT_SET_OUTPUT_MODULE)
        self.module_name = module_name
        self.module = None

    def voice_manager(self):
        return self.get_module().voice_manager

    def set_voice(self, voice_name):
        try:
            voice = self.voice_manager().voice(voice_name)
            self.language = voice.get_language(0)
            self.physical_voice = voice.name
            self.logical_voice = self.voice_manager().logical_name(self.language, voice.name)
        except VoiceNotFoundError as ex:
            raise ProtocolError('{}: unknown voice'.format(voice_name), msgs.ERR_COULDNT_SET_VOICE) from ex

    def set_language(self, language):
        try:
            voice_info = self.voice_manager().main_voice_for_language(language)
            self.language = str(language)
            self.physical_voice = voice_info.voice.name
            self.logical_voice = voice_info.logical_name
        except VoiceNotFoundError as ex:
            raise ProtocolError('{}: unknown language'.format(language), msgs.ERR_COULDNT_SET_LANGUAGE) from ex

    def set_default_language(self):
        isolang = locale.getlocale()[0]
        self.set_language(isolang[0:2] if isolang else 'en')

    def set_logical_voice(self, logical_name):
        try:
            voice = self.voice_manager().logical_voice(self.language, logical_name)
            self.physical_voice = voice.name
            self.logical_voice = logical_name
        except VoiceNotFoundError as ex:
            raise ProtocolError('{}: unknown voice'.format(logical_name), msgs.ERR_COULDNT_SET_VOICE) from ex

    def list(self, name):
        if name == 'OUTPUT_MODULES':
            return sorted([ name for name in self.modules ])
        elif name == 'SYNTHESIS_VOICES':
            return [ '{} {} {}'.format(voice.name, voice.get_language(0)[0:2], voice.get_variant(0))
                     for voice in self.voice_manager().voices() ]
        elif name == 'VOICES':
            return self.voice_manager().MANDATORY_LOGICAL_VOICES
        raise ProtocolError('{}: invalid parameter'.format(name), msgs.ERR_PARAMETER_INVALID)

    def get(self, name):
        if name == 'CAP_LET_RECOGN':
            return self.cap_let_recogn
        elif name == 'LANGUAGE':
            return self.language
        elif name == 'OUTPUT_MODULE':
            return self.module_name
        elif name == 'PITCH':
            return self.pitch
        elif name == 'PUNCTUATION':
            return self.punctuation
        elif name == 'RATE':
            return self.rate
        elif name == 'SPELLING':
            return self.spelling
        elif name == 'SSML_MODE':
            return self.from_boolean(self.ssml_mode)
        elif name == 'SYNTHESIS_VOICE':
            return self.physical_voice
        elif name == 'VOICE' or name == 'VOICE_TYPE':
            return self.logical_voice
        elif name == 'VOLUME':
            return self.volume
        raise ProtocolError('{}: invalid parameter'.format(name), msgs.ERR_PARAMETER_INVALID)

    def set(self, name, value):
        if name == 'CAP_LET_RECOGN':
            value = self.in_list(value, self.CAP_LET_RECOGN_CHOICES, msgs.ERR_COULDNT_SET_CAP_LET_RECOG)
            if value != self.CAP_LET_RECOGN_CHOICES[0]:
                raise ProtocolError('{}: parameter not supported'.format(name), msgs.ERR_NOT_IMPLEMENTED)
            self.cap_let_recogn = value
        elif name == 'LANGUAGE':
            self.set_language(str(value))
        elif name == 'OUTPUT_MODULE':
            self.set_module(value)
        elif name == 'PAUSE_CONTEXT':
            raise ProtocolError('{}: parameter not supported'.format(name), msgs.ERR_NOT_IMPLEMENTED)
        elif name == 'PITCH':
            self.pitch = self.in_range(value, OutputParameters.MIN_PITCH, OutputParameters.MAX_PITCH, msgs.ERR_PITCH_TOO_LOW, msgs.ERR_PITCH_TOO_HIGH, msgs.ERR_COULDNT_SET_PITCH)
        elif name == 'PUNCTUATION':
            self.punctuation = self.in_list(value, self.PUNCTUATION_CHOICES, msgs.ERR_COULDNT_SET_PUNCTUATION)
        elif name == 'RATE':
            self.rate = self.in_range(value, OutputParameters.MIN_RATE, OutputParameters.MAX_RATE, msgs.ERR_RATE_TOO_LOW, msgs.ERR_RATE_TOO_HIGH, msgs.ERR_COULDNT_SET_RATE)
        elif name == 'SPELLING':
            self.spelling = self.to_boolean(value)
        elif name == 'SSML_MODE':
            self.ssml_mode = self.to_boolean(value)
        elif name == 'SYNTHESIS_VOICE':
            self.set_voice(value)
        elif name == 'VOICE' or name == 'VOICE_TYPE':
            self.set_logical_voice(value.upper())
        elif name == 'VOLUME':
            self.volume = self.in_range(value, OutputParameters.MIN_VOLUME, OutputParameters.MAX_VOLUME, msgs.ERR_VOLUME_TOO_LOW, msgs.ERR_VOLUME_TOO_HIGH, msgs.ERR_COULDNT_SET_VOLUME)
        else:
            raise ProtocolError('{}: invalid parameter'.format(name), msgs.ERR_PARAMETER_INVALID)

Message = collections.namedtuple('Message', ['data', 'parameters'])

class MessageBlock:

    """Block of messages for the priority queue"""

    def __init__(self, client_number, priority, closed):
        self.client_number = client_number
        self.priority = priority
        self.pending = []
        self.processed = []
        self.queued = False
        self.closed = closed

    def append(self, message):
        self.pending.append(message)

    def is_empty(self):
        return not self.pending

    def is_terminated(self):
        return self.closed and self.is_empty()

    def pop(self):
        if self.pending:
            message = self.pending.pop(0)
            self.processed.append(message)
            return message

class PriorityQueue:

    """Manage a queue of items with priority.

    A lower value means higher priority."""

    PRY_NAME_IMPORTANT = 'IMPORTANT'
    PRY_NAME_MESSAGE = 'MESSAGE'
    PRY_NAME_TEXT = 'TEXT'
    PRY_NAME_NOTIFICATION = 'NOTIFICATION'
    PRY_NAME_PROGRESS = 'PROGRESS'

    PRIORITY_CHOICES = (PRY_NAME_IMPORTANT, PRY_NAME_MESSAGE, PRY_NAME_TEXT, PRY_NAME_NOTIFICATION, PRY_NAME_PROGRESS)

    PRY_IMPORTANT = PRIORITY_CHOICES.index(PRY_NAME_IMPORTANT)
    PRY_MESSAGE = PRIORITY_CHOICES.index(PRY_NAME_MESSAGE)
    PRY_TEXT = PRIORITY_CHOICES.index(PRY_NAME_TEXT)
    PRY_NOTIFICATION = PRIORITY_CHOICES.index(PRY_NAME_NOTIFICATION)
    PRY_PROGRESS = PRIORITY_CHOICES.index(PRY_NAME_PROGRESS)

    def __init__(self):
        self.queue = []
        self.current = None

    def next(self):
        if self.queue:
            self.current = self.queue.pop(0)
        return self.current

    def find_if(self, predicate):
        """Find items that matches."""
        return [ item for item in self.queue if predicate(item) ]

    def remove_if(self, predicate):
        """Remove items that matches."""
        self.queue = [ item for item in self.queue if not predicate(item) ]

    def insert(self, new_item):
        """Insert a new item, returns True if the current item must be canceled."""
        priority = new_item.priority
        canceled = False
        if priority < self.PRY_TEXT:
            # notification and progress are canceled
            self.remove_if(lambda item: item.priority > self.PRY_TEXT)
            if self.current and self.current.priority > priority:
                canceled = True
        elif priority == self.PRY_TEXT:
            # notification and progress are canceled and all previous text
            self.remove_if(lambda item: item.priority > self.PRY_MESSAGE)
            if self.current and self.current.priority >= priority:
                canceled = True
        elif priority == self.PRY_NOTIFICATION:
            self.remove_if(lambda item: item.priority == self.PRY_NOTIFICATION)
            if self.current and self.current.priority == self.PRY_NOTIFICATION:
                canceled = True
            elif self.current or self.queue:
                return False # only spoken if there is no other item in the queue
        elif priority == self.PRY_PROGRESS:
            self.remove_if(lambda item: item.priority > self.PRY_TEXT)
            if (self.current and self.current.priority < self.PRY_NOTIFICATION) or self.queue:
                return False
        index = 0
        while index < len(self.queue):
            item = self.queue[index]
            if item.priority > priority:
                break
            index += 1
        self.queue.insert(index, new_item)
        new_item.queued = True
        return canceled

    def clear_current(self):
        """Clear current item."""
        self.current = None

    def clear_all(self):
        """Remove all items in the queue."""
        self.queue = []

    @classmethod
    def priority_value(self, name):
        priority = Parameters.in_list(name.upper(), self.PRIORITY_CHOICES, msgs.ERR_UNKNOWN_PRIORITY)
        return self.PRIORITY_CHOICES.index(priority)


class Client:

    """Client for processing each server connection."""

    def __init__(self, number, name=None):
        self.number = number
        self.name = name or ''
        self.priority = PriorityQueue.PRY_MESSAGE
        self.parameters = OutputParameters()
        self.parameters.set_default_language()
        self.stream = None
        self.message_id = 0
        self.terminated = False
        self.block = None
        self.l10n = {}

    def get_localizations(self):
        language = self.parameters.language.split('-')[0]
        try:
            return self.l10n[language]
        except KeyError:
            pass
        try:
            mod = importlib.import_module('speakerd.l10n.{}'.format(language))
            self.l10n[language] = mod
            return mod
        except ImportError:
            self.l10n[language] = None # Avoid reloading the missing module
        return None

    def get_parameter(self, name):
        if name == 'CLIENT_NAME':
            return self.name
        elif name == 'PRIORITY':
            return PriorityQueue.PRIORITY_CHOICES[self.priority]
        return self.parameters.get(name)

    def set_parameter(self, name, value):
        if self.block and not name in [ 'RATE', 'PITCH', 'VOLUME', 'VOICE', 'VOICE_TYPE', 'LANGUAGE', 'PUNCTUATION', 'CAP_LET_RECOGN' ]:
            raise ProtocolError('{}: invalid inside block'.format(name), msgs.ERR_NOT_ALLOWED_INSIDE_BLOCK)
        if name == 'CLIENT_NAME':
            self.name = value
        elif name == 'PRIORITY':
            self.priority = PriorityQueue.priority_value(value)
        else:
            self.parameters.set(name, value)

    def list_parameters(self, name):
        return self.parameters.list(name)

    def get_output_module(self):
        return self.parameters.get_module()

    def has_stream(self):
        return self.stream != None

    def open_stream(self):
        self.stream = io.BytesIO()

    def write_data(self, data):
        self.stream.write(data)

    def feed_block(self, data):
        logging.debug("client #{}: new message: {}".format(self.number, data[0:20]))
        msg = Message(data, self.parameters.clone())
        if self.block:
            block = self.block
        else:
            block = MessageBlock(self.number, self.priority, True)
        block.append(msg)
        return block

    def __get_localization(self, scope, code):
        localizations = self.get_localizations()
        try:
            return getattr(localizations, scope)[code]
        except KeyError:
            return code

    def __make_translated_block(self, scope, code):
        if self.has_stream():
            raise ProtocolError('sound icon before end of stream', msgs.ERR_INVALID_COMMAND)
        name = self.__get_localization(scope, code)
        return self.feed_block(name.encode(Parameters.ENCODING))

    def fetch_char(self, char_code):
        return self.__make_translated_block('characters', char_code)

    def fetch_key(self, key_code):
        name = ' '.join([ self.__get_localization('keys', code) for code in key_code.split('_') ])
        return self.feed_block(name.encode(Parameters.ENCODING))

    def fetch_sound_icon(self, icon_code):
        return self.__make_translated_block('icons', icon_code)

    def enter_block(self):
        if self.block:
            raise ProtocolError('already inside a block', msgs.ERR_ALREADY_INSIDE_BLOCK)
        self.block = MessageBlock(self.number, self.priority, False)
        return self.block

    def fetch_block(self):
        data = self.stream
        self.stream = None
        return self.feed_block(data.getvalue())

    def exit_block(self):
        if not self.block:
            raise ProtocolError('not inside a block', msgs.ERR_ALREADY_OUTSIDE_BLOCK)
        self.block.closed = True
        self.block = None

    def next_message_id(self):
        self.message_id += 1
        return self.message_id


class ClientHandler:

    """Client side processing.

    This is a singleton that dispatches the commands to the different clients."""

    PARAMETERS = OutputConstants()

    def __init__(self, config):
        self.clients = {}
        self.output_queue = PriorityQueue()
        self.logging_level = logging.WARNING
        Parameters.current_pitch = int(config.get('output', 'pitch', defval=self.PARAMETERS.DEFAULT_PITCH))
        Parameters.current_rate = int(config.get('output', 'rate', defval=self.PARAMETERS.DEFAULT_RATE))
        self.number_of_messages = 0
        OutputParameters.initialize_modules(config)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        current_client = self.get_current_client()
        if current_client:
            current_client.get_output_module().stop()
        OutputParameters.clear_modules()

    @staticmethod
    def detect_voices():
        """Return a dictionary of voices by modules and languages."""
        result = {}
        voices_by_module = available_voices()
        for module_name in voices_by_module:
            result_by_language = result[module_name] = {}
            voices_by_language = voices_by_module[module_name]
            for language in voices_by_language:
                result_by_language[language] = result_by_name = {}
                voices_by_name = voices_by_language[language]
                for logical_name in voices_by_name:
                    result_by_name[logical_name] = voices_by_name[logical_name].name
        return result

    def get_client(self, client_number):
        try:
            client = self.clients[client_number]
            client.terminated = False
        except KeyError:
            self.clients[client_number] = client = Client(client_number)
        return client

    def get_current_block(self):
        """Current block plus the corresponding client"""
        block = self.output_queue.current
        client = None
        if block:
            client_number = block.client_number
            if client_number in self.clients:
                client = self.clients[client_number]
        return block, client

    def get_current_client(self):
        """Client currently playing a message"""
        block, client = self.get_current_block()
        return client

    def get_target(self, client, target):
        """Return the current client if it matches the target."""
        if target == 'ALL':
            return self.get_current_client()
        else:
            if target == 'SELF':
                client_number = client.number
            else:
                try:
                    client_number = int(target)
                except ValueError as ex:
                    raise ProtocolError('{}: not a client number'.format(target), msgs.ERR_PARAMETER_INVALID) from ex
            if self.output_queue.current and self.output_queue.current.client_number == client_number:
                return self.get_current_client()
        return None

    def detach(self, client_number):
        """Mark the client for deletion."""
        try:
            client = self.clients[client_number]
            client.terminated = True
            if not self.output_queue.find_if(lambda item: item.client_number == client_number):
                logging.debug('removing client #{}'.format(client_number))
                del self.clients[client_number]
        except KeyError:
            pass

    def set_debug(self, is_on):
        root = logging.getLogger()
        level = root.getEffectiveLevel()
        if is_on and level != logging.DEBUG:
            self.logging_level = level
            root.setLevel(logging.DEBUG)
        elif not is_on and level == logging.DEBUG:
            root.setLevel(self.logging_level)

    def stop_output(self, client, target):
        current_client = self.get_target(client, target)
        if current_client:
            current_client.get_output_module().stop()
        return msgs.OK_STOPPED

    def cancel_output(self, client, target):
        self.stop_output(client, target)
        self.output_queue.clear_all()
        return msgs.OK_CANCELED

    def pause_output(self, client, target):
        current_client = self.get_target(client, target)
        if current_client:
            current_client.get_output_module().pause()
        return msgs.OK_PAUSED

    def resume_output(self, client, target):
        current_client = self.get_target(client, target)
        if current_client:
            current_client.get_output_module().resume()
        return msgs.OK_RESUMED

    def process_set_request(self, client, target, name, *values):
        target = target.upper()
        if target == 'SELF':
            clients = [ client ]
        elif target == 'ALL':
            clients = [ client for client in self.clients.values() ]
        else:
            try:
                client_number = int(target)
                if not client_number in self.clients:
                    raise ValueError('{}: not found'.format(client_number))
                clients = [ self.clients[client_number] ]
            except ValueError as ex:
                raise ProtocolError('{}: not a client number'.format(name), msgs.ERR_PARAMETER_INVALID) from ex

        if target != 'ALL':
            if name in [ 'DEBUG' ]:
                ssip_error = eval('msgs.ERR_COULDNT_SET_{}'.format(name))
                raise ProtocolError('{}: can only be set on all'.format(name), ssip_error)
        if len(clients) > 1 or clients[0].number != client.number: # Not self
            if client.block:
                raise ProtocolError('{}: invalid inside block'.format(name), msgs.ERR_NOT_ALLOWED_INSIDE_BLOCK)
            if name in [ 'CLIENT_NAME', 'PRIORITY', 'SSML_MODE', 'NOTIFICATION' ]:
                ssip_error = eval('msgs.ERR_COULDNT_SET_{}'.format(name))
                raise ProtocolError('{}: can only be set on itself'.format(name), ssip_error)

        name = name.upper()
        if name == 'NOTIFICATION':
            notification, value = values
            value = Parameters.to_boolean(value)
            raise ProtocolError('{}: notification not implemented'.format(notification), msgs.ERR_NOT_IMPLEMENTED)
        elif name == 'HISTORY':
            Parameters.to_boolean(values[-1])
            raise ProtocolError('{}: history not implemented'.format(values[0]), msgs.ERR_NOT_IMPLEMENTED)
        elif name == 'DEBUG':
            (value,) = values
            self.set_debug(Parameters.to_boolean(value))
        else:
            (value,) = values
            for client in clients:
                client.set_parameter(name, value)
        return eval('msgs.OK_{}_SET'.format(name))

    def process_block_request(self, client, direction):
        if direction == 'BEGIN':
            client.enter_block()
            return msgs.OK_INSIDE_BLOCK
        elif direction == 'END':
            client.exit_block()
            return msgs.OK_OUTSIDE_BLOCK
        return msgs.ERR_INVALID_COMMAND

    def process_block(self, block):
        current_block, current_client = self.get_current_block()
        is_idle = True
        if current_client:
            is_idle = current_client.get_output_module().proceed()
            if is_idle and current_client.terminated and current_block.closed:
                self.detach(current_client.number)
        if is_idle and current_block and current_block.is_terminated():
            self.output_queue.clear_current()
            current_block = None
        if block:
            self.number_of_messages += 1
            if not block.queued and self.output_queue.insert(block):
                # The block has been queued and cancel the current one
                logging.debug('client: cancelling current message')
                if current_client:
                    current_client.get_output_module().stop()
        if is_idle:
            if not current_block or current_block.is_terminated():
                self.output_queue.next()
                current_block, current_client = self.get_current_block()
            if current_block and not current_block.is_empty():
                msg = current_block.pop()
                logging.debug('client: playing: {}'.format(msg.data[0:20]))
                current_client.get_output_module().say(msg)

    def process_speak_request(self, client):
        self.process_block(client.fetch_block())
        message_id = client.next_message_id()
        return [ '{}-{}'.format(msgs.C_OK_MESSAGE_QUEUED, message_id), msgs.OK_MESSAGE_QUEUED ]

    def process_sound_icon_request(self, client, icon_name):
        self.process_block(client.fetch_sound_icon(icon_name))
        return msgs.OK_SND_ICON_QUEUED

    def process_char_request(self, client, char_name):
        self.process_block(client.fetch_char(char_name))
        return msgs.OK_MESSAGE_QUEUED

    def process_key_request(self, client, key_name):
        self.process_block(client.fetch_key(key_name))
        return msgs.OK_MESSAGE_QUEUED

    def process_request(self, client, raw_request):
        request = raw_request.decode(Parameters.ENCODING).rstrip()
        logging.debug('client #{} request: {}'.format(client.number, request))
        args = shlex.split(request)
        command = args.pop(0).upper()
        keep = True
        if command == 'BLOCK':
            answer = self.process_block_request(client, args[0].upper())
        elif command == 'SET':
            answer = self.process_set_request(client, *tuple(args))
        elif command == 'SPEAK':
            client.open_stream()
            answer = msgs.OK_RECEIVE_DATA
        elif command == 'SOUND_ICON':
            answer = self.process_sound_icon_request(client, *tuple(args))
        elif command == 'CHAR':
            answer = self.process_char_request(client, *tuple(args))
        elif command == 'KEY' :
            answer = self.process_key_request(client, *tuple(args))
        elif command == 'QUIT':
            answer = msgs.OK_BYE
            self.detach(client.number)
            keep = False
        elif client.block:
            answer = msgs.ERR_NOT_ALLOWED_INSIDE_BLOCK
        elif command == 'LIST':
            name = args.pop(0)
            tag = eval('msgs.C_OK_{}_LIST'.format(name))
            answer = [ '{}-{}'.format(tag, value) for value in client.list_parameters(name.upper()) ]
            answer.append(eval('msgs.OK_{}_LIST_SENT'.format(name)))
        elif command == 'GET':
            name = args.pop(0)
            value = client.get_parameter(name)
            answer = [ '{}-{}'.format(msgs.C_OK_GET, value), msgs.OK_GET ]
        elif command == 'STOP':
            answer = self.stop_output(client, args[0].upper() if args else 'SELF')
        elif command == 'CANCEL':
            answer = self.cancel_output(client, args[0].upper() if args else 'SELF')
        elif command == 'PAUSE':
            answer = self.pause_output(client, args[0].upper() if args else 'SELF')
        elif command == 'RESUME':
            answer = self.resume_output(client, args[0].upper() if args else 'SELF')
        elif command in [ 'HELP', 'HISTORY' ]:
            answer = msgs.ERR_NOT_IMPLEMENTED
        else:
            answer = msgs.ERR_INVALID_COMMAND
        return answer, keep

    def proceed(self):
        """Process the next message if possible."""
        self.process_block(None)

    def handle(self, client_number, rfile, wfile):
        client = self.get_client(client_number)
        input_line = True
        number_of_lines = 0
        number_of_messages = self.number_of_messages
        keep = True
        while input_line:
            input_line = rfile.readline()
            if input_line:
                number_of_lines += len(input_line)
                answer = None
                try:
                    if client.has_stream():
                        if input_line == b'.\r\n':
                            answer = self.process_speak_request(client)
                        else:
                            client.write_data(input_line)
                    else:
                        answer, keep = self.process_request(client, input_line)
                except ProtocolError as ex:
                    logging.exception("SSIP error: {}".format(ex.ssip_error))
                    answer = ex.ssip_error
                except NotImplementedError as ex:
                    logging.exception("not implemented")
                    answer = msgs.ERR_NOT_IMPLEMENTED
                except Exception as ex:
                    logging.exception("internal error")
                    answer = msgs.ERR_INTERNAL
                if answer:
                    if isinstance(answer, str):
                        answer = [ answer ]
                    for output_line in answer:
                        logging.debug('client #{} answer: {}'.format(client.number, output_line))
                        data = '{}\r\n'.format(output_line).encode(Parameters.ENCODING)
                        wfile.write(data)
                    wfile.flush()
        if self.number_of_messages == number_of_messages:
            # no message added, so cleaning up the last spoken message
            self.proceed()
        return keep and number_of_lines > 0
