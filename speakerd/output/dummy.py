# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = []

class PlayBack:

    NAME = 'dummy'

    def __init__(self, config, voice_manager, **kw):
        self.voice_manager = voice_manager
        voice_config = config.get('modules', 'dummy', 'voices')
        if voice_config:
            self.voice_manager.initialize(voice_config)

    def proceed(self):
        """Do any pending action and return if idle."""
        return True

    def say(self, msg):
        """Say the message"""
        pass

    def stop(self):
        pass

    def pause(self):
        pass

    def resume(self):
        pass

    def is_available(self):
        return True
