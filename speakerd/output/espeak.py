# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import locale
import logging
import os
import re
import subprocess

__all__ = []

from .base import PlayBackWithCache, find_command
from .cache import DelayedOutputCache
from .voices import Voice

class Voices:

    separator_re = re.compile(r'\s\s+') # at least two whitespaces

    other_lang_re = re.compile(r'\(([a-z]{2}(?:-[a-z]{2})?) (\d+)\)')

    def __init__(self, config=None):
        self.voices = []
        espeak_command = find_command('espeak')
        if config:
            espeak_command = config.get('modules', 'espeak', 'commands', 'espeak', defval=espeak_command)
        self.espeak_command = espeak_command

    def __parse_output(self, cmdline, by_name, all_languages):
        """Execute espeak to list the voices and parses the output."""
        encoding = locale.getpreferredencoding()
        with subprocess.Popen(cmdline, stdin=subprocess.DEVNULL, stdout=subprocess.PIPE) as proc:
            for bline in proc.stdout:
                if bline.startswith(b'Pty'):
                    continue
                line = bline.decode(encoding).strip()
                columns = line.split(maxsplit=5)
                try:
                    if len(columns) == 5:
                        priority, language, age_gender, name, filename = tuple(columns)
                        other_languages = None
                    else:
                        priority, language, age_gender, name, filename, other_languages = tuple(columns)
                    gender = age_gender.split('/')[-1]
                except ValueError as ex:
                    logging.error('{}: {}'.format(ex, line))
                    continue
                name = name.lower()
                if name in by_name:
                    voice = by_name[name]
                else:
                    voice = by_name[name] = Voice(name, gender)
                all_languages[language] = True
                voice.add_language(language, int(priority))
                if other_languages:
                    for language, priority in self.other_lang_re.findall(other_languages):
                        voice.add_language(language, int(priority))
                        if not language in all_languages:
                            all_languages[language] = False

    def load(self):
        """Load the list of languages supported by espeak."""
        if not self.voices:
            all_languages = {}
            logging.debug('loading espeak voices')
            by_name = {}
            self.__parse_output([ self.espeak_command, '--voices' ], by_name, all_languages)
            initial_languages = [ language for language in all_languages ]
            for language in initial_languages:
                if len(language) == 2:
                    # The list of voices may not be complete
                    logging.debug('loading espeak voices for language {}'.format(language))
                    self.__parse_output([ self.espeak_command, '--voices={}'.format(language) ], by_name, all_languages)
            self.voices = by_name.values()
        return self.voices

class PlayBack(PlayBackWithCache):

    ESPEAK_MIN_AMPLITUDE = 0
    ESPEAK_MAX_AMPLITUDE = 200

    ESPEAK_MIN_PITCH = 0
    ESPEAK_MAX_PITCH = 100

    ESPEAK_MIN_SPEED = 80
    ESPEAK_DEFAULT_SPEED = 175
    ESPEAK_MAX_SPEED = 450

    SOME_PUNCT = ",.;:!?"

    NAME = 'espeak'

    def __init__(self, config, voice_manager, quiet=False):
        super().__init__(config, self.NAME, DelayedOutputCache)
        self.espeak_command = config.get('modules', self.NAME, 'commands', 'espeak', defval=find_command('espeak'))
        self.quiet = quiet # For tests
        self.voice_manager = voice_manager
        voice_config = config.get('modules', self.NAME, 'voices')
        if voice_config:
            self.voice_manager.initialize(voice_config)
        else:
            self.voice_manager.add_voices(Voices().load())

    @classmethod
    def transform_range(self, value, source_min, source_max, dest_min, dest_max):
        ratio = (dest_max - dest_min) / (source_max - source_min)
        return int((value - source_min) * ratio + dest_min)

    @classmethod
    def convert_volume(self, parameters):
        return self.transform_range(parameters.volume,
                                    parameters.MIN_VOLUME, parameters.MAX_VOLUME,
                                    self.ESPEAK_MIN_AMPLITUDE, self.ESPEAK_MAX_AMPLITUDE)

    @classmethod
    def convert_pitch(self, parameters):
        return self.transform_range(parameters.pitch,
                                    parameters.MIN_PITCH, parameters.MAX_PITCH,
                                    self.ESPEAK_MIN_PITCH, self.ESPEAK_MAX_PITCH)

    @classmethod
    def convert_rate(self, parameters):
        """Convert SSPI rate to espeak speed.

        Convertion is not linear because the default speed is not the middle between min and max."""
        rate = parameters.rate
        if rate < parameters.DEFAULT_RATE:
            return self.transform_range(rate,
                                        parameters.MIN_RATE, parameters.DEFAULT_RATE,
                                        self.ESPEAK_MIN_SPEED, self.ESPEAK_DEFAULT_SPEED)
        return self.transform_range(rate,
                                    parameters.DEFAULT_RATE, parameters.MAX_RATE,
                                    self.ESPEAK_DEFAULT_SPEED, self.ESPEAK_MAX_SPEED)

    def is_available(self):
        return self.espeak_command != None

    def produce(self, msg, output_path=None):
        """Produce the output."""
        cmdline = [ self.espeak_command,
                    '-z', # no pause at end of sentence
                    '-a', str(self.convert_volume(msg.parameters)),
                    '-p', str(self.convert_pitch(msg.parameters)),
                    '-s', str(self.convert_rate(msg.parameters)),
                    '-v', msg.parameters.physical_voice ]
        if self.quiet:
            cmdline.append('-q')
        if msg.parameters.ssml_mode:
            cmdline.append('-m')
        if msg.parameters.punctuation == 'some':
            cmdline.append('--punct={}'.format(self.SOME_PUNCT))
        elif msg.parameters.punctuation == 'all':
            cmdline.append('--punct')
        if output_path:
            cmdline.append('-w')
            cmdline.append(output_path)
        stdin, stdout = self.spawn_process(cmdline, True)
        stdin.write(msg.data)
        stdin.close()
