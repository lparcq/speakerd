# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import collections
import logging
import re
import subprocess

__all__ = [ 'VoiceManager', 'Voice', 'VoiceNotFoundError' ]

class Voice:

    def __init__(self, name, gender):
        self.name = name
        self.gender = gender
        self.languages = []

    def add_language(self, language, priority):
        if not language in [ pair[0] for pair in self.languages ]:
            self.languages.append((language, priority))

    def number_of_languages(self):
        return len(self.languages)

    def get_language(self, index):
        return self.languages[index][0]

    def get_variant(self, index):
        language = self.get_language(index)
        if len(language) == 2:
            return 'none'
        return language

    def terminate(self):
        """Sort languages by priority."""
        self.languages.sort(key=lambda pair: '{:03d}/{}'.format(pair[1], pair[0]))

    def __str__(self):
        return "{} {} {}".format(self.name, self.gender, ''.join(['({} {})'.format(language, priority) for language, priority in self.languages]))

class VoiceNotFoundError(Exception):
    pass

class VoiceInfo:

    __slots__ = ('voice', 'logical_name', 'priority')

    def __init__(self, voice, logical_name, priority):
        self.voice = voice
        self.logical_name = logical_name
        self.priority = int(priority)

    def __lt__(self, other):
        return self.priority < other.priority

class VoiceManager:

    MANDATORY_LOGICAL_VOICES = [ 'CHILD_FEMALE', 'CHILD_MALE',
                                 'FEMALE1', 'FEMALE2', 'FEMALE3',
                                 'MALE1', 'MALE2', 'MALE3' ]

    logical_name_re = re.compile(r'([A-Z_]+)(\d*)$')

    def __init__(self):
        self.by_name = {}
        self.by_language = {} # lang -> [ VoiceInfo(voice, logical_name, priority), ... ]

    @classmethod
    def logical_name_sort_key(self, logical_name):
        """Convert a name to a key for sorting.

        Ex: MALE1 -> 001MALE"""
        try:
            m = self.logical_name_re.match(logical_name)
        except TypeError as ex:
            raise TypeError('{}: invalid logical name'.format(logical_name)) from ex
        if not m:
            raise ValueError('{}: invalid logical name'.format(logical_name))
        rank = m.group(2) or '0'
        return '{:03d}{}'.format(int(rank), m.group(1))

    def initialize(self, config):
        """Initialize a list of voices grouped by language and logical names."""
        config = config.as_dict()
        for language in config:
            in_language = config[language]
            self.by_language[language] = by_language = []
            priority = 1
            for logical_name in sorted(in_language, key=self.logical_name_sort_key):
                name = in_language[logical_name]
                gender = 'F' if logical_name.startswith('FEMALE') or logical_name.startswith('CHILD_FEMALE') else 'M'
                voice = Voice(name, gender)
                voice.add_language(language, priority)
                self.by_name[name] = voice
                by_language.append(VoiceInfo(voice, logical_name, priority))
                priority += 1

    def languages(self):
        return [ language for language in self.by_language ]

    def voices(self):
        return self.by_name.values()

    def voice(self, name):
        """Return the voice with this logical name."""
        try:
            return self.by_name[name]
        except KeyError as ex:
            raise VoiceNotFoundError('{}: voice not found'.format(name)) from ex

    def main_voice_for_language(self, language):
        """Return the main voice info for a language."""
        try:
            return self.by_language[language][0]
        except Exception as ex:
            raise VoiceNotFoundError('{}: voice not found for this language'.format(language)) from ex

    def voices_for_language(self, language):
        """Return voices for LANGUAGE grouped by logical name."""
        voices = collections.OrderedDict()
        if language in self.by_language:
            for voice_info in sorted(self.by_language[language], key=lambda x: x.logical_name):
                voices[voice_info.logical_name] = voice_info.voice
        return voices

    def voices_by_language(self, *languages):
        """Return voices grouped by language and by logical name."""
        voices = collections.OrderedDict()
        for language in sorted(list(languages) or self.by_language):
            voices[language] = self.voices_for_language(language)
        return voices

    def logical_voice(self, language, logical_name):
        """Return a voice for a language by logical name (FEMALE1, MALE1, ...)"""
        try:
            for voice_info in self.by_language[language]:
                if voice_info.logical_name == logical_name:
                    return voice_info.voice
        except KeyError:
            pass
        if logical_name in self.MANDATORY_LOGICAL_VOICES:
            voice_info = self.main_voice_for_language(language)
            return voice_info.voice
        raise VoiceNotFoundError('{}: voice not found for language {}'.format(logical_name, language))

    def logical_name(self, language, voice_name):
        """Return the logical name of a voice for a language"""
        try:
            for voice_info in self.by_language[language]:
                if voice_info.voice.name == voice_name:
                    return voice_info.logical_name
        except KeyError:
            pass
        raise VoiceNotFoundError('{}: logical name of this voice not found for language {}'.format(voice_name, language))

    def add_voice(self, new_voice):
        """Add a new voice."""
        new_voice.terminate()
        self.by_name[new_voice.name] = new_voice
        # Insert the voice in all language list it belongs
        for language, priority in new_voice.languages:
            voice_list = self.by_language.setdefault(language, [])
            voice_list.append(VoiceInfo(new_voice, None, priority))
            voice_list.sort() # sort by priority
            # Update logical names
            male_index = female_index = 1
            for voice_info in voice_list:
                voice = voice_info.voice
                if voice.gender == 'M' or (voice.gender != 'F' and male_index <= female_index):
                    voice_info.logical_name = 'MALE{}'.format(male_index)
                    male_index += 1
                else:
                    voice_info.logical_name = 'FEMALE{}'.format(female_index)
                    female_index += 1

    def add_voices(self, voices):
        for voice in voices:
            self.add_voice(voice)
