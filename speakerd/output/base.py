# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import logging
import os
import subprocess

__all__ = []

def find_command(filename):
    """Find executable in the path."""
    if os.path.isfile(filename):
        return filename
    for dirname in os.environ['PATH'].split(os.pathsep):
        path = os.path.join(dirname, filename)
        if os.path.isfile(path):
            return path
    return None

class PlayBackWithCache:

    DEFAULT_MAX_ENTRIES = 20

    def __init__(self, config, name, cache_factory):
        self.play_command = config.get('commands', 'play')
        self.proc = None
        cache_dir = config.get('cache', 'directory')
        if not cache_dir:
            raise ValueError('cache directory is null')
        self.cache_dir = os.path.join(cache_dir, name)
        self.max_cache_entries = config.get('cache', 'max_entries', defval=self.DEFAULT_MAX_ENTRIES)
        self.cache_factory = cache_factory
        self.caches = {}
        self.cached_output = None
        self.paused = False

    def get_cached_data(self, msg):
        """Get the cache corresponding to the msg parameters.

        The cache doesn't depend on the language but only on the voice."""
        params = { 'voice': msg.parameters.physical_voice, 'pitch': msg.parameters.pitch, 'rate': msg.parameters.rate,
                   'volume': msg.parameters.volume, 'cap': msg.parameters.cap_let_recogn,
                   'spelling': 1 if msg.parameters.spelling else 0,
                   'ssml': 1 if msg.parameters.ssml_mode else 0 }
        cache_key = 'VC{voice}%PI{pitch}%RT{rate}%VL{volume}%CA{cap}%SP{spelling}%SS{ssml}'.format(**params)
        if cache_key in self.caches:
            cache = self.caches[cache_key]
        else:
            cache_dir = os.path.join(self.cache_dir, cache_key)
            if not os.path.isdir(cache_dir):
                parent_dir = os.path.dirname(cache_dir)
                if not os.path.isdir(parent_dir):
                    os.mkdir(parent_dir)
                os.mkdir(cache_dir)
            self.caches[cache_key] = cache = self.cache_factory(cache_dir, self.max_cache_entries)
        return cache.get(msg.data)

    def is_idle(self):
        """Check there is no running child process."""
        if self.proc:
            retcode = self.proc.poll()
            if retcode == None:
                return False
            elif retcode != 0:
                logging.error("command failed with code {}: {}".format(retcode, ' '.join(self.proc.args)))
        self.paused = False
        return True

    def proceed(self):
        """Do any pending action and return if idle."""
        is_idle = self.is_idle()
        if is_idle and self.cached_output:
            cached_output = self.cached_output
            self.cached_output = None
            cached_output.update()
            self.play(cached_output.path)
            is_idle = self.is_idle()
        return is_idle

    def spawn_process(self, cmdline, with_stdin=False, with_stdout=False):
        logging.debug('spawning: {}'.format(cmdline))
        stdin = subprocess.PIPE if with_stdin else subprocess.DEVNULL
        stdout = subprocess.PIPE if with_stdout else subprocess.DEVNULL
        self.proc = subprocess.Popen(cmdline, stdin=stdin, stdout=stdout)
        return (self.proc.stdin, self.proc.stdout)

    def play(self, filename):
        """Play the audio file."""
        subprocess.call([ self.play_command, filename ])

    def say(self, msg):
        """Say the message"""
        cached_output = self.get_cached_data(msg)
        if cached_output.exists():
            self.play(cached_output.path)
        elif cached_output.is_cacheable():
            self.cached_output = cached_output
            self.produce(msg, cached_output.path)
        else:
            self.produce(msg)

    def stop(self):
        self.cached_output = None # cancel current message if being cached
        if not self.is_idle() and self.proc:
            self.resume()
            proc = self.proc
            self.proc = None
            logging.debug('espeak: terminating process')
            proc.terminate()
            logging.debug('espeak: waiting for process to terminate')
            try:
                proc.wait(1)
            except subprocess.TimeoutExpired:
                logging.debug('espeak: killing process')
                proc.kill()
                logging.debug('espeak: waiting for process to terminate')
                proc.wait()

    def pause(self):
        if not self.paused and not self.is_idle() and self.proc:
            logging.debug('espeak: sending signal SIGSTOP to process')
            self.proc.send_signal(signal.SIGSTOP)
            self.paused = True

    def resume(self):
        if self.paused and not self.is_idle() and self.proc:
            logging.debug('espeak: sending signal SIGCONT to process')
            self.proc.send_signal(signal.SIGCONT)
        self.paused = False
