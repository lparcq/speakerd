# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import logging
import os
import selectors
import signal
import socket
import threading

__all__ = [ 'Server' ]

class SignalHandler:

    def __init__(self, fun):
        self.fun = fun

    def __call__(self, signum, frame):
        logging.info('signal {} received'.format(signum))
        thread = threading.Thread(target=self.fun, daemon=True)
        thread.start()

class ServerError(Exception):
    pass

class Server:

    select_timeout = None

    def __init__(self, config, client_handler):
        socket_path = config.get('socket_path')
        if socket_path:
            self.address = socket_path
            self.family = socket.AF_UNIX
        else:
            self.address = (config.get('host'), config.get('port', defval=6560))
            self.family = socket.AF_INET
            if not self.address[0]:
                raise Server("Invalid configuration, missing 'socket_path' or 'host'")
        self.max_clients = config.get('max_clients')
        self.client_handler = client_handler
        self.signal_handler = SignalHandler(self.interrupt)
        self.child_handler = SignalHandler(self.wake_up)
        self.files = {}
        self.started = False
        self.start_condition = threading.Condition()
        self.done = False
        self.selector = selectors.DefaultSelector()
        # signaling pipe
        self.signaling = os.pipe2(os.O_NONBLOCK)
        self.selector.register(self.signaling[0], selectors.EVENT_READ, self.on_signaling)
        # main socket
        sock = socket.socket(self.family, socket.SOCK_STREAM)
        self.sock = self.__register_socket(sock, self.accept)
        # signals
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGCHLD, self.child_handler)

    def __enter__(self):
        self.sock.bind(self.address)
        self.address = self.sock.getsockname()
        self.sock.listen(self.max_clients)
        return self

    def __exit__(self, *exc):
        self.close()

    def __register_socket(self, sock, callback):
        sock.setblocking(False)
        self.selector.register(sock, selectors.EVENT_READ, callback)
        return sock

    def __unregister_socket(self, sock):
        self.selector.unregister(sock)
        client_number = sock.fileno()
        try:
            sock.shutdown(socket.SHUT_WR)
        except OSError:
            pass
        sock.close()

    def accept(self, sock, mask):
        sock, address = sock.accept()
        logging.info('new client {}'.format(address or 'pipe'))
        rfile = sock.makefile('rb')
        wfile = sock.makefile('wb')
        client_number = sock.fileno()
        self.files[client_number] = (sock, rfile, wfile)
        logging.debug('registering client #{}'.format(client_number))
        self.__register_socket(sock, self.read)

    def interrupt(self):
        """Write a byte to wake up the selector and set the ."""
        if self.signaling:
            logging.debug('server: interrupting by signaling socket')
            os.write(self.signaling[1], b'1')
        else:
            logging.debug('server: interrupting')
            self.done = True

    def wake_up(self):
        """Write a byte to wake up the selector."""
        if self.signaling:
            logging.debug('server: waking up')
            os.write(self.signaling[1], b'0')

    def on_signaling(self, fd, mask):
        """Data received on the signaling pipe."""
        data = os.read(fd, 1)
        if data == b'1':
            self.done = True
        self.client_handler.proceed()

    def read(self, sock, mask):
        client_number = sock.fileno()
        try:
            sock_copy, rfile, wfile = self.files[client_number]
            keep = True
            try:
                keep = self.client_handler.handle(client_number, rfile, wfile)
            except:
                logging.exception('unexpected client exception')
                keep = False
            if not keep:
                self.close_client(client_number)
        except KeyError:
            logging.error('{}: unknown file descriptor'.format(client_number))

    def close_client(self, client_number):
        logging.info("unregistering client #{}".format(client_number))
        try:
            self.client_handler.detach(client_number)
        except:
            logging.exception('unexpected client exception')
        sock, rfile, wfile = self.files[client_number]
        del self.files[client_number]
        self.__unregister_socket(sock)

    def close(self):
        for client_number in [ n for n in self.files ]:
            self.close_client(client_number)
        self.__unregister_socket(self.sock)
        os.close(self.signaling[0])
        os.close(self.signaling[1])
        self.signaling = None
        if self.family == socket.AF_UNIX:
            os.unlink(self.address)

    def wait(self, timeout=None):
        if not self.started:
            with self.start_condition:
                self.start_condition.wait(timeout)

    def run(self):
        logging.info('starting server on {}'.format(self.address))
        self.started = True
        with self.start_condition:
            self.start_condition.notify_all()
        while not self.done:
            events = self.selector.select(self.select_timeout)
            for key, mask in events:
                callback = key.data
                callback(key.fileobj, mask)
        self.started = False
