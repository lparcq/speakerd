# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
characters = {
    '!': "point d'exclamation",
    '"': "guillemet",
    '#': "dièse",
    '$': "dollar",
    '%': "pourcent",
    '&': "et commercial",
    "'": "apostrophe",
    '(': "parenthèse ouvrante",
    ')': "parenthèse fermante",
    '*': "étoile",
    '+': "plus",
    ',': "virgule",
    '-': "moins",
    '.': "point",
    '/': "divisé",
    ':': "deux points",
    ';': "point virgule",
    '<': "inférieur",
    '=': "égal",
    '>': "supérieur",
    '?': "point d'intérogation",
    '@': "arobase",
    '[': "crochet ouvrant",
    '\\': "barre oblique inversée",
    ']': "crochet fermant",
    '^': "chapeau",
    '_': "souligné",
    '`': "accent grave",
    '{': "accolade ouvrante",
    '|': "barre verticale",
    '}': "accolade fermante",
    '~': "tilde",
}

icons = {
    'beginning-of-line': 'début de ligne',
    'empty-text': 'texte vide',
    'end-of-line': 'fin de ligne',
    'finish': 'fin',
    'message': 'message',
    'prompt': 'invite de saisie',
    'start': 'début',
    'whitespace': 'blanc'
}

keys = {
    # Special keys
    'space': 'espace',
    'underscore': 'souligné',
    'double-quote': 'guillemet',
    # Auxiliary Keys
    'alt': 'alt',
    'control': 'controle',
    'hyper': 'hyper',
    'meta': 'meta',
    'shift': 'majuscule',
    'super': 'super',
    # Control Character Keys
    'backspace': 'effacement',
    'break': 'interruption',
    'delete': 'suppression',
    'down': 'bas',
    'end': 'fin',
    'enter': 'entrée',
    'escape': 'échappement',
    'f1': 'f 1',
    'f2': 'f 2',
    'f3': 'f 3',
    'f4': 'f 4',
    'f5': 'f 5',
    'f6': 'f 6',
    'f7': 'f 7',
    'f8': 'f 8',
    'f9': 'f 9',
    'f10': 'f 10',
    'f11': 'f 11',
    'f12': 'f 12',
    'f13': 'f 13',
    'f14': 'f 14',
    'f15': 'f 15',
    'f16': 'f 16',
    'f17': 'f 17',
    'f18': 'f 18',
    'f19': 'f 19',
    'f20': 'f 20',
    'f21': 'f 21',
    'f22': 'f 22',
    'f23': 'f 23',
    'f24': 'f 24',
    'home': 'débit',
    'insert': 'insérer',
    'kp-*': 'pavé star',
    'kp-+': 'pavé plus',
    'kp--': 'pavé minus',
    'kp-.': 'pavé dot',
    'kp-/': 'pavé slash',
    'kp-0': 'pavé 0',
    'kp-1': 'pavé 1',
    'kp-2': 'pavé 2',
    'kp-3': 'pavé 3',
    'kp-4': 'pavé 4',
    'kp-5': 'pavé 5',
    'kp-6': 'pavé 6',
    'kp-7': 'pavé 7',
    'kp-8': 'pavé 8',
    'kp-9': 'pavé 9',
    'kp-enter': 'pavé enter',
    'left': 'gauche',
    'menu': 'menu',
    'next': 'suivant',
    'num-lock': 'vérrouillage numérique',
    'pause': 'pause',
    'print': 'imprimer',
    'prior': 'précédent',
    'return': 'retour',
    'right': 'droit',
    'scroll-lock': 'arrêt défillement',
    'tab': 'tabulation',
    'up': 'haut',
    'window': 'logo',
}

# Keys that represents characters said the same
keys.update(characters)
