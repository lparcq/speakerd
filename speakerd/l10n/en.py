# Copyright (C) 2016 Laurent Pelecq <lpelecq+speakerd@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

characters = {
    '!': "exclamation mark",
    '"': "double quote",
    '#': "pound",
    '$': "dollar",
    '%': "percent",
    '&': "ampersand",
    "'": "quote",
    '(': "opening parenthesis",
    ')': "closing parenthesis",
    '*': "star",
    '+': "plus",
    ',': "comma",
    '-': "minus",
    '.': "dot",
    '/': "slash",
    ':': "colon",
    ';': "semi colon",
    '<': "less than",
    '=': "equal",
    '>': "greater than",
    '?': "question mark",
    '@': "at sign",
    '[': "opening square bracket",
    '\\': "backslash",
    ']': "closing square bracket",
    '^': "caret",
    '_': "underscore",
    '`': "backtick",
    '{': "opening bracket",
    '|': "vertical bar",
    '}': "closing bracket",
    '~': "tilda",
}

icons = {
    'beginning-of-line': 'beginning of line',
    'empty-text': 'empty text',
    'end-of-line': 'end of line',
    'finish': 'finish',
    'message': 'message',
    'prompt': 'prompt',
    'start': 'start',
    'whitespace': 'whitespace'
}

keys = {
    # Special keys
    'space': 'space',
    'underscore': 'underscore',
    'double-quote': 'double quote',
    # Auxiliary Keys
    'alt': 'alt',
    'control': 'control',
    'hyper': 'hyper',
    'meta': 'meta',
    'shift': 'shift',
    'super': 'super',
    # Control Character Keys
    'backspace': 'backspace',
    'break': 'break',
    'delete': 'delete',
    'down': 'down',
    'end': 'end',
    'enter': 'enter',
    'escape': 'escape',
    'f1': 'f 1',
    'f2': 'f 2',
    'f3': 'f 3',
    'f4': 'f 4',
    'f5': 'f 5',
    'f6': 'f 6',
    'f7': 'f 7',
    'f8': 'f 8',
    'f9': 'f 9',
    'f10': 'f 10',
    'f11': 'f 11',
    'f12': 'f 12',
    'f13': 'f 13',
    'f14': 'f 14',
    'f15': 'f 15',
    'f16': 'f 16',
    'f17': 'f 17',
    'f18': 'f 18',
    'f19': 'f 19',
    'f20': 'f 20',
    'f21': 'f 21',
    'f22': 'f 22',
    'f23': 'f 23',
    'f24': 'f 24',
    'home': 'home',
    'insert': 'insert',
    'kp-*': 'keypad star',
    'kp-+': 'keypad plus',
    'kp--': 'keypad minus',
    'kp-.': 'keypad dot',
    'kp-/': 'keypad slash',
    'kp-0': 'keypad 0',
    'kp-1': 'keypad 1',
    'kp-2': 'keypad 2',
    'kp-3': 'keypad 3',
    'kp-4': 'keypad 4',
    'kp-5': 'keypad 5',
    'kp-6': 'keypad 6',
    'kp-7': 'keypad 7',
    'kp-8': 'keypad 8',
    'kp-9': 'keypad 9',
    'kp-enter': 'keypad enter',
    'left': 'left',
    'menu': 'menu',
    'next': 'next',
    'num-lock': 'num lock',
    'pause': 'pause',
    'print': 'print',
    'prior': 'prior',
    'return': 'return',
    'right': 'right',
    'scroll-lock': 'scroll lock',
    'tab': 'tab',
    'up': 'up',
    'window': 'window',
}

# Keys that represents characters said the same
keys.update(characters)
